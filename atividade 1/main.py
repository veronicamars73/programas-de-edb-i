import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

sns.set()

file_reg = open("registros.txt")

tempos_alg1l = []
tempos_alg2l = []

tempos_alg1 = []
tempos_alg2 = []


for linha in file_reg:
	campos = linha.split(";")
	if (campos[0]=="algor1"):
		tempos_alg1l.append(np.log(float(campos[1].replace("\n", ""))))
		tempos_alg1.append(float(campos[1].replace("\n", "")))
	if (campos[0]=="algor2"):
		tempos_alg2l.append(np.log(float(campos[1].replace("\n", ""))))
		tempos_alg2.append(float(campos[1].replace("\n", "")))

dfl = pd.DataFrame({"Algoritimo Contagem um por um":tempos_alg1l,"Algoritimo Contagem soma de PA": tempos_alg2l})

print(dfl)

ax = dfl.plot.bar(title="Comparação de algoritmos")

plt.title("Comparação de algoritmos")
plt.ylabel("Tempo até fim da função em Log de n segundos")
plt.xlabel("Número da Execução")

plt.show()


df = pd.DataFrame({"Algoritimo Contagem um por um":tempos_alg1,"Algoritimo Contagem soma de PA": tempos_alg2})

print(df)

ax = df.plot.bar(title="Comparação de algoritmos")

plt.title("Comparação de algoritmos")
plt.ylabel("Tempo até fim da função em n segundos")
plt.xlabel("Número da Execução")

plt.show()


dfavg = {"Algoritimo Contagem um por um":np.average(tempos_alg1), "Algoritimo Contagem soma de PA": np.average(tempos_alg2)}

print(dfavg)

alg = list(dfavg.keys())
tempoalg = list(dfavg.values())

plt.bar(alg, tempoalg,  width = 0.4) 

plt.title("Comparação de algoritmos")
plt.ylabel("Tempo médio até fim da função em n segundos")
plt.xlabel("Algoritimo")

plt.show()