#include <iostream> 

#define registros 5


unsigned long long contagemOBY(unsigned long long n){
	unsigned long long sum = 0;
	for (unsigned long long i = 1; i <= n; ++i) {
		sum = sum + i;
	}
	return sum;
}

unsigned long long contagemAP(unsigned long long n){
	return (n*(n+1))/2;
}


int main() { 

	unsigned long long numberToSum = 1000000000ULL;

	contagemAP(numberToSum);

	contagemOBY(numberToSum);


    return 0;
} 