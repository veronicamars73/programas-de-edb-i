#include <iostream> 
#include <chrono> 
#include <ctime>
#include <fstream>

#define registros 5


unsigned long long contagemOBY(unsigned long long n){
	unsigned long long sum = 0;
	for (unsigned long long i = 1; i <= n; ++i) {
		sum = sum + i;
	}
	return sum;
}

unsigned long long contagemAP(unsigned long long n){
	return (n*(n+1))/2;
}


int main() { 

	unsigned long long numberToSum = 1000000000ULL;

	std::ofstream archive("registros.txt");
    if(archive.fail()){
        std::cerr << "File creation not avaliable";
    }else{
    	for (int i = 0; i < registros; ++i)
    	{
    		std::chrono::time_point<std::chrono::system_clock> start, end; 
		    start = std::chrono::system_clock::now();
		    contagemOBY(numberToSum);
		    end = std::chrono::system_clock::now(); 
		    std::chrono::duration<double> elapsed_seconds = end - start; 
		    archive << "algor1;" << elapsed_seconds.count() << "\n"; 

		    start = std::chrono::system_clock::now();
		    contagemAP(numberToSum);
		    end = std::chrono::system_clock::now();
		    elapsed_seconds = end - start;
		    archive << "algor2;" << elapsed_seconds.count() << "\n"; 
    	}
    }


    return 0;
} 