# Atividade 1 de EDB I

Projeto criado como método de avaliação de presença para a disciplina de *Estruturas de Dados Básicas 1* no período de 2020.5.

# Informações Importantes

## Compilação

Compile o programa utilizando o comando `g++ -std=c++11 main.cpp` no terminal dentro do diretório do programa. 

## Instruções de uso

A implementação das formas de contagem estão no arquivo `main.cpp` e o arquivo que exibe o gráfico é o `main.py`.

Para usar o arquivo `main.cpp` o compile e execute o arquivo gerado com o comando `./<nome_do_arquivo>`.

> Lembre-se que o programa precisa ter permissões para a criação de arquivos

Para usar o `main.py` primeiro faça a instalação dos requisitos de funcionamento do programa (presente no arquivo `requirements.txt`) com o comando:

> O Arquivo main.py precisa do arquivo registros.txt, que é gerado na execução do arquivo gerado na compilação do arquivo main.cpp, para ser executado. Caso deseje, os gráficos estão disponíveis nesse documento e podem ser visto sem a execução do programa

``` shell
$  pip install -r requirements.txt
```
ou, para o python 3.7

``` shell
$  pip3.7 install -r requirements.txt
```

Após isso execute o programa `main.py` (o uso do python 3.7 é indicado) com o comando

``` shell
$  python main.py
```

ou, para o python 3.7

``` shell
$  pip3.7 install -r requirements.txt
```


# Análise de Algoritmos

## Número de Instruções

A função que conta a soma dos valores é a seguinte:

```cpp
unsigned long long contagemOBY(unsigned long long n){
	unsigned long long sum = 0;
	for (unsigned long long i = 1; i <= n; ++i) {
		sum = sum + i;
	}
	return sum;
}
```

Essa função contém 2n+2 instruções.

Já a função que usa o somatório de uma Progressão Aritmética para fazer a contagem é a seguinte: 

```cpp
unsigned long long contagemAP(unsigned long long n){
	return (n*(n+1))/2;
}
```

Essa função contém quatro instruções.

## Gráficos de Comparação de Tempo de Execução

Para comparar o tempo de execução dos arquivos usamos a biblioteca do `Python` `Matplotlib` para a criação de gráficos que apresentam o tempo de execução dos algoritmos, esse estão disponíveis no diretório `graficos`, nesse documento e de forma interativa na execução do arquivo `main.py`.

### Primeiro Gráfico: Tempos em Execuções Usando Log de n Segundos

O primeiro gráfico gerado apresenta 5 execuções dos algoritmos em Log do tempo de execução por segundo.

![Primeiro Gráfico: Tempos em Execuções Usando Log de n Segundos - Fonte: Própria](graficos/log_s.png)

*Primeiro Gráfico: Tempos em Execuções Usando Log de n Segundos - Fonte: Própria*

### Segundo Gráfico: Tempos em Execuções Usando n Segundos

O segundo gráfico gerado apresenta 5 execuções dos algoritmos com seus tempos de execução por segundo.

![Segundo Gráfico: Tempos em Execuções Usando n Segundos - Fonte: Própria](graficos/segundos.png)

*Segundo Gráfico: Tempos em Execuções Usando n Segundos - Fonte: Própria*

### Terceiro Gráfico: Média de Tempo de Execução de Cada Algoritmo

O Terceiro gráfico gerado a média de tempo de execução de cada um dos algoritmos.

![Terceiro Gráfico: Média de Tempo de Execução de Cada Algoritmo - Fonte: Própria](graficos/segundos.png)

*Terceiro Gráfico: Média de Tempo de Execução de Cada Algoritmo - Fonte: Própria*



# Autoria

Programa desenvolvido por **Alaide Lisandra Melo Carvalho** (<mendie73@gmail.com>) como projeto para a disciplina de *Linguagem de Programação I* de 2020.5.

&copy; IMD/UFRN 2020.