# Lista de Exercícios

## Complexidade Assintótica

### Questão 1

Informe qual a complexidade assintótica das implementações realizadas na semana passada:

- a) Soma de 1 até n (sendo n = 1.000.000.000) utilizando um loop for.
```cpp
unsigned long long contagemOBY(unsigned long long n){
    unsigned long long sum = 0;
    for (unsigned long long i = 1; i <= n; ++i) {
        sum = sum + i;
    }
    return sum;
}

```

> A implementação superior possui uma complexidade assintótica de **O(n)**.

- b) Soma de 1 até n (sendo n = 1.000.000.000) utilizando a fórmula de progressão aritmética de razão 1.

```cpp
unsigned long long contagemAP(unsigned long long n){
    return (n*(n+1))/2;
}
```

> A implementação superior possui uma complexidade assintótica de **O(1)**.

### Questão 2

Implemente uma função que retorna qual é o menor valor em um array de inteiros de tamanho n. Informe:

- a) Quantas instruções a função possui.

```cpp
int menor(int * n, int size) {
    int menorn = n[0];
    for (int i = 1; i < size; ++i) {
        if(menorn > n[i]){
            menorn = n[i];
        }
    }
    return menorn;
}
```
> A função possui 4n - 1 sendo n = size

- b) Qual a complexidade assintótica da função.

> A complexidade assintótica da função é **O(n)**.


### Questão 3

Dado um array A de números inteiros de tamanho n e um inteiro s, implemente uma função que retorna verdadeiro, se s está contido no array A, ou falso do contrário. Informe:

- a) Quantas instruções a função possui.

```cpp
bool exist(int buscado, int * n, int size) {
    for (int i = 0; i < size; ++i) {
        if(buscado == n[i]){
            return true;
        }
    }
    return false;
}
```

> A implementação superior possui n² + 2n instruções.

- b) Qual a complexidade assintótica da função.

> A complexidade assintótica da função é **O(n)**.

### Questão 4

Qual a complexidade assintótica das seguintes funções:

- a) f(n) = 25

> A complexidade assintótica da função é **O(1)**.

- b) f(n) = 48n

> A complexidade assintótica da função é **O(n)**.

- c) f(n) = 48n + 25

> A complexidade assintótica da função é **O(n)**.

- d) f(n) = 2n² + 100n + 33

> A complexidade assintótica da função é **O(n²)**.

- e) f(n) = 50n³ + 100n! + 33 log n

> A complexidade assintótica da função é **O(n!)**.

- f) f(n) = 35n⁴ + 25n ∗ 12 log n

> A complexidade assintótica da função é **O(n⁴)**.

- g) f(n) = 2 + 2n + 2n² + 2n³

> A complexidade assintótica da função é **O(n³)**.

- h) f(n) = n² + 3n⁵ + 4n²

> A complexidade assintótica da função é **O(n⁵)**.

- i) f(n) = 10^n + 1024n⁵

> A complexidade assintótica da função é **O(10^n)**.


# Autoria

Programa desenvolvido por **Alaide Lisandra Melo Carvalho** (<mendie73@gmail.com>) como projeto para a disciplina de *Estruturas de Dados Básicas 1* de 2020.6.

&copy; IMD/UFRN 2020.