# Atividade 2 de EDB I

Projeto criado como método de avaliação de presença para a disciplina de *Estruturas de Dados Básicas 1* no período de 2020.6.

# Informações Importantes

## Compilação

Compile qualquer programa utilizando o comando `g++ -std=c++11 <nomedoarquivo>.cpp` no terminal dentro do diretório do programa. 

## Instruções de uso

Para executar qualquer programa o compile e execute o arquivo gerado com o comando `./<nome_do_arquivo>`.

> Nenhum dos arquivos precisa de permissões especiais

# Autoria

Programa desenvolvido por **Alaide Lisandra Melo Carvalho** (<mendie73@gmail.com>) como projeto para a disciplina de *Estruturas de Dados Básicas 1* de 2020.6.

&copy; IMD/UFRN 2020.